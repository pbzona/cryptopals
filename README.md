# CryptoPals

This project consists of solutions to the [CryptoPals](https://cryptopals.com/)
challenges. Don't peek if you don't want spoilers :)
