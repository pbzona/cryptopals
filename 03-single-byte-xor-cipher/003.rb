require_relative "../lib/convert_encoding"
require_relative "../lib/xor"
require_relative "../lib/characters"

ciphertext = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'

readable_chars = generate_readable_chars
fq = generate_frequency_table

ciphertext_bytes = hex_to_bin(ciphertext).bytes
high_score = 0
likely_key = ""

readable_chars.each do |code|
  char = code.chr
  c = single_char_xor(ciphertext_bytes, char.bytes)
  key_score = 0

  c.each do |byte|
    key_score = readable_chars.include?(byte) ? key_score + 1 : key_score
  end

  if key_score >= high_score
    high_score = key_score
    likely_key = char
  end
end

# Results:
puts "Encryption key: #{likely_key}"
puts "Plaintext: #{single_char_xor(ciphertext_bytes, likely_key.bytes).pack("c*")}"
