require_relative "../lib/convert_encoding"

input = '49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d'
expected = 'SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t'

def hex_to_base64(hex)
  b = hex_to_bin(hex)
  bin_to_base64(b)
end

correct = hex_to_base64(input) == expected

puts "======="
puts hex_to_base64(input)
puts expected
puts "======="

raise "Nope" unless correct
puts "Yep!" if correct
